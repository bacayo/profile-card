# Frontend Mentor - Profile card component solution

This is a solution to the [Profile card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/profile-card-component-cfArpWshJ). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)

## Overview

### The challenge

- Build out the project to the designs provided

### Screenshot

![](./ss.png)

### Links

- Solution URL: [gitlab](https://gitlab.com/bacayo/profile-card/-/tree/main/profile-card-component-main)
- Live Site URL: [Live Link](https://profile-nine-black.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

### What I learned

I learned how to use flexbox. I am more confident about using custom css properties.

### Continued development

- Grid, js, DOM

### Useful resources

- [Mozilla Developer Network](https://developer.mozilla.org/en-US/) - I always find myself here befor stackoverflow.
- [W3schools](https://www.w3schools.com/) - Good resource
